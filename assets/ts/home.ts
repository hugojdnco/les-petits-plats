import { Api } from './classes/Core/Api.js';
import { Dom } from './classes/Core/Dom.js';
import { Tools } from './classes/Tools.js';
import { Ingredient } from './classes/Entity/Ingredient.js';
import { Recette } from './classes/Entity/Recette.js';
import { Ustencile } from './classes/Entity/Ustencile.js';
import { Appareil } from './classes/Entity/Appareil.js';

let recettes: Recette[];
let ingredients: Ingredient[];
let appareils: Appareil[];
let ustenciles: Ustencile[];
let listItem: NodeList | null;
let listTagsDeleteIcon: NodeList | null;
let ingredientsFiltered: Ingredient[] = [];
let recettesFiltered: Recette[] = [];
let appareilsFiltered: Appareil[] = [];
let ustencilesFiltered: Ustencile[] = [];

async function init() {
    recettes = await Api.getRecettes();
    if (recettes) {
        showRecettes(recettes);
    }

    defaultList();
}

async function defaultList() {
    ingredients = await Api.getIngredients();
    const ingredientsToShow: Ingredient[] = ingredients.slice(0, 30);
    if (ingredients) {
        fillListIngredients(ingredientsToShow);
    }

    appareils = await Api.getAppareils();
    const appareilsToShow: Appareil[] = appareils.slice(0, 30);
    if (appareils) {
        fillListAppareils(appareilsToShow);
    }

    ustenciles = await Api.getUstenciles();
    const ustencilesToShow: Ustencile[] = ustenciles.slice(0, 30);
    if (ustenciles) {
        fillListUstenciles(ustencilesToShow);
    }
    initAddEventListenerTag();
}

function showRecettes(recettesToShow: Recette[]) {
    const domRecettes: string = Dom.getDomRecettes(recettesToShow);
    const recettesSection: HTMLElement | null = document.querySelector('.list-recettes');
    if (recettesSection) {
        recettesSection.innerHTML = '';
        recettesSection.insertAdjacentHTML('beforeend', domRecettes);
    }
}

function fillListIngredients(ingredients: Ingredient[]) {
    const domListIngredients: string = Dom.getDomListIngredients(ingredients);
    const listIngredients: HTMLElement | null = document.querySelector('.list-ingredient');
    if (listIngredients) {
        listIngredients.innerHTML = '';
        listIngredients.insertAdjacentHTML('beforeend', domListIngredients);
    }
}

function fillListAppareils(appareils: Appareil[]) {
    const domListAppareils: string = Dom.getDomListAppareils(appareils);
    const listAppareils: HTMLElement | null = document.querySelector('.list-appareil');
    if (listAppareils) {
        listAppareils.innerHTML = '';
        listAppareils.insertAdjacentHTML('beforeend', domListAppareils);
    }
}

function fillListUstenciles(ustenciles: Ustencile[]) {
    const domListUstenciles: string = Dom.getDomListUstenciles(ustenciles);
    const listUstenciles: HTMLElement | null = document.querySelector('.list-ustencile');
    if (listUstenciles) {
        listUstenciles.innerHTML = '';
        listUstenciles.insertAdjacentHTML('beforeend', domListUstenciles);
    }
}

init();

/* SEARCH BAR */
const searchIcon: HTMLElement | null = document.querySelector('.fas.fa-search');
const searchInput: HTMLElement | null = document.querySelector('.global-search');
if (searchIcon) {
    searchIcon.addEventListener('click', function () {
        if (searchInput) {
            searchInput.focus();
        }
    });
}

/* SELECTORS */
const arrowSelect: NodeList | null = document.querySelectorAll('.selector .input i');
arrowSelect.forEach((selector: any) => selector.addEventListener('click', function (e: any) {
    toggleListSelect(e)
}));

function toggleListSelect(event: any) {
    const listToUse: any = document.querySelector('.' + event.target.getAttribute('data-list'));
    if (listToUse.classList.contains('hide')) {
        event.target.classList.add('rotated');
        listToUse.classList.remove('hide');
    } else {
        event.target.classList.remove('rotated');
        listToUse.classList.add('hide');
    }
}

/* PRINCIPAL SEARCH */
const inputFieldText: HTMLElement | null = document.querySelector('.global-search');
if (inputFieldText) {
    inputFieldText.addEventListener('keydown', function (e: any) {
        if (e.target.value.length > 1) {
            filterSearch(e.target.value);
        } else if (e.target.value.length == 0) {
            showRecettes(recettes);
            defaultList();
        }
    })
}

let listRecettesToUse: Recette[] = [];
async function filterSearch(value: string) {
    listRecettesToUse = recettesFiltered.length == 0 ? recettes : recettesFiltered;
    recettesFiltered = [];
    listRecettesToUse.forEach((recette: Recette) => {
        if (recette.name.toLowerCase().includes(value.toLowerCase()) || recette.description.toLowerCase().includes(value.toLowerCase())) {
            recettesFiltered.push(recette);
        } else {
            let recetteCorrespond = false;
            recette.ingredients.forEach((ingredient: any) => {
                if (ingredient.libelle.toLowerCase().includes(value.toLowerCase())) {
                    recettesFiltered.push(recette);
                    recetteCorrespond = true;
                }
            });

            if (recetteCorrespond == false) {
                if (recette.appareil.libelle.toLowerCase().includes(value.toLowerCase())) {
                    recettesFiltered.push(recette);
                    recetteCorrespond = true;
                }
            }

            if (recetteCorrespond == false) {
                recette.ustenciles.forEach((ustencile: any) => {
                    if (ustencile.libelle.toLowerCase().includes(value.toLowerCase())) {
                        recettesFiltered.push(recette);
                        recetteCorrespond = true;
                    }
                });
            }
        }
    });
    showRecettes(recettesFiltered);
    if (recettesFiltered.length) {
        resetListWithRecettesFiltered(recettesFiltered);
        initAddEventListenerTag();
    } else {
        alert('Aucune recette ne correspond à votre recherche');
    }
}

/* SEARCH IN INGREDIENTS, APPAREILS OR USTENCILS LIST */
const inputTextIngredient: HTMLElement | null = document.querySelector('input[name="ingredient"]');
if (inputTextIngredient) {
    inputTextIngredient.addEventListener('keydown', function (e: any) {
        if (e.target.value.length > 2) {
            filterSearchListIngredient(e.target.value);
        } else if (e.target.value.length == 0) {
            fillListIngredients(list_ingredients);
        }
        initAddEventListenerTag();
    })
}

async function filterSearchListIngredient(value: string) {
    ingredientsFiltered = [];
    list_ingredients.forEach((ingredient: Ingredient) => {
        if (ingredient.libelle.toLowerCase().includes(value.toLowerCase())) {
            ingredientsFiltered.push(ingredient);
        }
    })
    fillListIngredients(Tools.dedupe(ingredientsFiltered));
}

const inputTextAppareil: HTMLElement | null = document.querySelector('input[name="appareil"]');
if (inputTextAppareil) {
    inputTextAppareil.addEventListener('keydown', function (e: any) {
        if (e.target.value.length > 2) {
            filterSearchListAppareil(e.target.value);
        } else if (e.target.value.length == 0) {
            fillListAppareils(list_appareils);
        }
        initAddEventListenerTag();
    })
}

async function filterSearchListAppareil(value: string) {
    appareilsFiltered = [];
    list_appareils.forEach((appareil: Appareil) => {
        if (appareil.libelle.toLowerCase().includes(value.toLowerCase())) {
            appareilsFiltered.push(appareil);
        }
    })
    fillListAppareils(Tools.dedupe(appareilsFiltered));
}

const inputTextUstencile: HTMLElement | null = document.querySelector('input[name="ustencile"]');
if (inputTextUstencile) {
    inputTextUstencile.addEventListener('keydown', function (e: any) {
        if (e.target.value.length > 2) {
            filterSearchListUstencile(e.target.value);
        } else if (e.target.value.length == 0) {
            fillListUstenciles(list_ustenciles);
        }
        initAddEventListenerTag();
    })
}

async function filterSearchListUstencile(value: string) {
    ustencilesFiltered = [];
    list_ustenciles.forEach((ustencile: Ustencile) => {
        if (ustencile.libelle.toLowerCase().includes(value.toLowerCase())) {
            ustencilesFiltered.push(ustencile);
        }
    })
    fillListUstenciles(Tools.dedupe(ustencilesFiltered));
}

let list_ingredients: Ingredient[] = [];
let list_appareils: Appareil[] = [];
let list_ustenciles: Ustencile[] = [];
function resetListWithRecettesFiltered(recettesFiltered: Recette[]) {
    recettesFiltered.forEach((recette: Recette) => {
        const tmp_ingredients: Ingredient[] = [];
        recette.ingredients.forEach((ingredient: Ingredient) => {
            tmp_ingredients.push(ingredient);
        });
        list_ingredients = list_ingredients.concat(tmp_ingredients);

        const tmp_appareil: Appareil = recette.appareil;
        list_appareils = list_appareils.concat(tmp_appareil);

        const tmp_ustenciles: Ustencile[] = [];
        recette.ustenciles.forEach((ustencile: Ustencile) => {
            tmp_ustenciles.push(ustencile);
        })
        list_ustenciles = list_ustenciles.concat(tmp_ustenciles);
    })

    list_ingredients = Tools.dedupe(list_ingredients);
    list_appareils = Tools.dedupe(list_appareils);
    list_ustenciles = Tools.dedupe(list_ustenciles);

    /*recettesFiltered.map((recette: any) => {
        const tmp_ingredients = recette.ingredients.map((ingredient: any) => {
            return ingredient;
        })
        list_ingredients = list_ingredients.concat(tmp_ingredients);
    
        const tmp_appareil = recette.appareil;
        list_appareils = list_appareils.concat(tmp_appareil);
    
        const tmp_ustenciles = recette.ustenciles.map((ustencile: any) => {
            return ustencile;
        })
        list_ustenciles = list_ustenciles.concat(tmp_ustenciles);
    });*/
    fillListIngredients(list_ingredients);
    fillListAppareils(Tools.dedupe(list_appareils));
    fillListUstenciles(Tools.dedupe(list_ustenciles));
}

/* ADD OR REMOVE TAG IN SEARCH */
function initRemoveEventListenerTag() {
    listTagsDeleteIcon = document.querySelectorAll('.delete-tag');
    listTagsDeleteIcon.forEach((deleteIcon: any) => deleteIcon.addEventListener('click', function (e: any) {
        removeTag(e.target);
    }));
}

function initAddEventListenerTag() {
    listItem = document.querySelectorAll('.list-item span');
    listItem.forEach((item: any) => item.addEventListener('click', function (e: any) {
        addTag(e.target);
    }));
}

const listTagSection: HTMLElement | null = document.querySelector('.list-things-choosed');
function addTag(element: any) {
    const type_item: string = element.getAttribute('data-type');
    const tag_value: string = element.innerHTML;
    const dom_tag: string = Dom.getDomTag(tag_value, type_item);
    if (listTagSection) {
        listTagSection.insertAdjacentHTML('beforeend', dom_tag);
    }
    initRemoveEventListenerTag();
    filterSearch(tag_value);
}

function removeTag(element: any) {
    element.parentNode.remove();
}
