import { Ingredient } from './../Entity/Ingredient.js';
import { Recette } from './../Entity/Recette.js';
import { Ustencile } from './../Entity/Ustencile.js';
import { Appareil } from './../Entity/Appareil.js';

export class Dom {

    public static getDomRecettes(recettes: Recette[]) {
        let html = '';
        recettes.forEach((recette: Recette) => {
            html += `<div class="recette">
            <div class="img-container">
                <img src="./assets/images/recettes/recette1.jpeg" alt="" />
            </div>
            <div class="content-container">
                <div class="header-content">
                    <h2 class="recette-title">
                        ${recette.name}
                    </h2>
                    <div class="recette-time">
                        <i class="fas fa-clock"></i>
                        <span>${recette.time} min</span>
                    </div>
                </div>
                <div class="body-content">
                    <div class="list-ingredients">
                        <ul>`;
            recette.ingredients.forEach((ingredient: Ingredient) => {
                html += `<li><span>${ingredient.libelle} :</span> ${ingredient.quantity ? ingredient.quantity : ''} ${ingredient.unit ? ingredient.unit : ''}</li>`;
            });
            html += `</ul>
                    </div>
                    <div class="recette-text">
                        ${recette.description}
                    </div>
                </div>
            </div>
        </div>`;
        })
        return html;
    }

    public static getDomListIngredients(ingredients: Ingredient[]) {
        let html = '';
        ingredients.forEach((ingredient: Ingredient) => {
            html += `<div class="list-item">
                        <span data-type="ingredient">${ingredient.libelle}</span>
                    </div>`;
        })
        return html;
    }

    public static getDomListAppareils(appareils: Appareil[]) {
        let html = '';
        appareils.forEach((appareil: Appareil) => {
            html += `<div class="list-item">
                        <span data-type="appareil">${appareil.libelle}</span>
                    </div>`;
        })
        return html;
    }

    public static getDomListUstenciles(ustenciles: Ustencile[]) {
        let html = '';
        ustenciles.forEach((ustencile: Ustencile) => {
            html += `<div class="list-item">
                        <span data-type="ustencile">${ustencile.libelle}</span>
                    </div>`;
        })
        return html;
    }

    public static getDomTag(value: string, type: string) {
        let class_tag = '';
        if (type == 'ingredient') {
            class_tag = 'blue';
        } else if (type == 'appareil') {
            class_tag = 'green';
        } else if (type == 'ustencile') {
            class_tag = 'red';
        }

        let html = '';
        html += `<div class="thing ${class_tag}">
                    <span>${value}</span>
                    <i class="fas fa-times-circle delete-tag" > </i>
                </div>`;

        return html;
    }
}