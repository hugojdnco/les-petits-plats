
import { Ingredient } from './../Entity/Ingredient.js';
import { Recette } from './../Entity/Recette.js';
import { Ustencile } from './../Entity/Ustencile.js';
import { Appareil } from './../Entity/Appareil.js';
import { Tools } from './../Tools.js';

export class Api {
    private static urlApi = './assets/data/recettes.json';
    private static responseApi: string;
    private static recettes: Recette[] = [];
    private static ingredients: Ingredient[] = [];
    private static ustenciles: Ustencile[] = [];
    private static appareils: Appareil[] = [];

    public static async getResponse() {
        if (Api.responseApi) {
            return Api.responseApi;
        }

        const responseApi: any = await fetch(Api.urlApi);
        Api.responseApi = await responseApi.json();

        return Api.responseApi;
    }

    public static async getRecettes() {
        if (Api.recettes.length != 0) {
            return Api.recettes;
        }

        const response: any = await Api.getResponse();

        let allIngredients: Ingredient[] = [];
        const allAppareils: Appareil[] = [];
        let allUstenciles: Ustencile[] = [];


        response.recipes.forEach((recette: any) => {
            const ingredients: Ingredient[] = [];
            recette.ingredients.forEach((ingredient: any) => {
                ingredients.push(new Ingredient(ingredient));
            })
            allIngredients = allIngredients.concat(ingredients);


            const appareil: Appareil = new Appareil(recette.appliance);
            allAppareils.push(appareil);

            const ustenciles: Ustencile[] = [];
            recette.ustensils.forEach((ustencile: any) => {
                ustenciles.push(new Ustencile(ustencile));
            });
            allUstenciles = allUstenciles.concat(ustenciles);

            Api.recettes.push(new Recette(recette, ingredients, appareil, ustenciles));
        });

        Api.ingredients = Tools.dedupe(allIngredients);
        Api.appareils = Tools.dedupe(allAppareils);
        Api.ustenciles = Tools.dedupe(allUstenciles);

        return Api.recettes;
    }

    public static async getIngredients() {
        return Api.ingredients;
    }

    public static async getUstenciles() {
        return Api.ustenciles;
    }

    public static async getAppareils() {
        return Api.appareils;
    }
}