export class Ingredient {
    public libelle: string;
    public quantity: number;
    public unit: string;

    constructor(ingredient: any) {
        this.libelle = ingredient.ingredient;
        this.quantity = ingredient.quantity;
        this.unit = ingredient.unit;
    }
}