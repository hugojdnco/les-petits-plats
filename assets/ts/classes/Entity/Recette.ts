import { Ingredient } from './../Entity/Ingredient.js';
import { Ustencile } from './../Entity/Ustencile.js';
import { Appareil } from './../Entity/Appareil.js';

export class Recette {
    public id: number;
    public name: string;
    public servings: number;
    public time: number;
    public description: string;
    public ingredients: any;
    public appareil: any;
    public ustenciles: any;


    constructor(recette: any, ingredients: Ingredient[], appareil: Appareil, ustenciles: Ustencile[]) {
        this.id = recette.id;
        this.name = recette.name;
        this.servings = recette.servings;
        this.time = recette.time;
        this.description = recette.description;
        this.ingredients = ingredients;
        this.appareil = appareil;
        this.ustenciles = ustenciles;
    }
}