export class Tools {
    public static dedupe(arr: any, indexToCheck = 'libelle') {
        const obj: any = {};

        for (let i = 0, len = arr.length; i < len; i++)
            obj[arr[i][indexToCheck]] = arr[i];

        arr = [];
        for (const key in obj)
            arr.push(obj[key]);

        return arr;
    }

    public static firstLetterUppercase(str: string) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
} 