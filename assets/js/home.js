var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { Api } from './classes/Core/Api.js';
import { Dom } from './classes/Core/Dom.js';
import { Tools } from './classes/Tools.js';
let recettes;
let ingredients;
let appareils;
let ustenciles;
let listItem;
let listTagsDeleteIcon;
let ingredientsFiltered = [];
let recettesFiltered = [];
let appareilsFiltered = [];
let ustencilesFiltered = [];
function init() {
    return __awaiter(this, void 0, void 0, function* () {
        recettes = yield Api.getRecettes();
        if (recettes) {
            showRecettes(recettes);
        }
        defaultList();
    });
}
function defaultList() {
    return __awaiter(this, void 0, void 0, function* () {
        ingredients = yield Api.getIngredients();
        const ingredientsToShow = ingredients.slice(0, 30);
        if (ingredients) {
            fillListIngredients(ingredientsToShow);
        }
        appareils = yield Api.getAppareils();
        const appareilsToShow = appareils.slice(0, 30);
        if (appareils) {
            fillListAppareils(appareilsToShow);
        }
        ustenciles = yield Api.getUstenciles();
        const ustencilesToShow = ustenciles.slice(0, 30);
        if (ustenciles) {
            fillListUstenciles(ustencilesToShow);
        }
        initAddEventListenerTag();
    });
}
function showRecettes(recettesToShow) {
    const domRecettes = Dom.getDomRecettes(recettesToShow);
    const recettesSection = document.querySelector('.list-recettes');
    if (recettesSection) {
        recettesSection.innerHTML = '';
        recettesSection.insertAdjacentHTML('beforeend', domRecettes);
    }
}
function fillListIngredients(ingredients) {
    const domListIngredients = Dom.getDomListIngredients(ingredients);
    const listIngredients = document.querySelector('.list-ingredient');
    if (listIngredients) {
        listIngredients.innerHTML = '';
        listIngredients.insertAdjacentHTML('beforeend', domListIngredients);
    }
}
function fillListAppareils(appareils) {
    const domListAppareils = Dom.getDomListAppareils(appareils);
    const listAppareils = document.querySelector('.list-appareil');
    if (listAppareils) {
        listAppareils.innerHTML = '';
        listAppareils.insertAdjacentHTML('beforeend', domListAppareils);
    }
}
function fillListUstenciles(ustenciles) {
    const domListUstenciles = Dom.getDomListUstenciles(ustenciles);
    const listUstenciles = document.querySelector('.list-ustencile');
    if (listUstenciles) {
        listUstenciles.innerHTML = '';
        listUstenciles.insertAdjacentHTML('beforeend', domListUstenciles);
    }
}
init();
/* SEARCH BAR */
const searchIcon = document.querySelector('.fas.fa-search');
const searchInput = document.querySelector('.global-search');
if (searchIcon) {
    searchIcon.addEventListener('click', function () {
        if (searchInput) {
            searchInput.focus();
        }
    });
}
/* SELECTORS */
const arrowSelect = document.querySelectorAll('.selector .input i');
arrowSelect.forEach((selector) => selector.addEventListener('click', function (e) {
    toggleListSelect(e);
}));
function toggleListSelect(event) {
    const listToUse = document.querySelector('.' + event.target.getAttribute('data-list'));
    if (listToUse.classList.contains('hide')) {
        event.target.classList.add('rotated');
        listToUse.classList.remove('hide');
    }
    else {
        event.target.classList.remove('rotated');
        listToUse.classList.add('hide');
    }
}
/* PRINCIPAL SEARCH */
const inputFieldText = document.querySelector('.global-search');
if (inputFieldText) {
    inputFieldText.addEventListener('keydown', function (e) {
        if (e.target.value.length > 1) {
            filterSearch(e.target.value);
        }
        else if (e.target.value.length == 0) {
            showRecettes(recettes);
            defaultList();
        }
    });
}
let listRecettesToUse = [];
function filterSearch(value) {
    return __awaiter(this, void 0, void 0, function* () {
        listRecettesToUse = recettesFiltered.length == 0 ? recettes : recettesFiltered;
        recettesFiltered = [];
        listRecettesToUse.forEach((recette) => {
            if (recette.name.toLowerCase().includes(value.toLowerCase()) || recette.description.toLowerCase().includes(value.toLowerCase())) {
                recettesFiltered.push(recette);
            }
            else {
                let recetteCorrespond = false;
                recette.ingredients.forEach((ingredient) => {
                    if (ingredient.libelle.toLowerCase().includes(value.toLowerCase())) {
                        recettesFiltered.push(recette);
                        recetteCorrespond = true;
                    }
                });
                if (recetteCorrespond == false) {
                    if (recette.appareil.libelle.toLowerCase().includes(value.toLowerCase())) {
                        recettesFiltered.push(recette);
                        recetteCorrespond = true;
                    }
                }
                if (recetteCorrespond == false) {
                    recette.ustenciles.forEach((ustencile) => {
                        if (ustencile.libelle.toLowerCase().includes(value.toLowerCase())) {
                            recettesFiltered.push(recette);
                            recetteCorrespond = true;
                        }
                    });
                }
            }
        });
        showRecettes(recettesFiltered);
        if (recettesFiltered.length) {
            resetListWithRecettesFiltered(recettesFiltered);
            initAddEventListenerTag();
        }
        else {
            alert('Aucune recette ne correspond à votre recherche');
        }
    });
}
/* SEARCH IN INGREDIENTS, APPAREILS OR USTENCILS LIST */
const inputTextIngredient = document.querySelector('input[name="ingredient"]');
if (inputTextIngredient) {
    inputTextIngredient.addEventListener('keydown', function (e) {
        if (e.target.value.length > 2) {
            filterSearchListIngredient(e.target.value);
        }
        else if (e.target.value.length == 0) {
            fillListIngredients(list_ingredients);
        }
        initAddEventListenerTag();
    });
}
function filterSearchListIngredient(value) {
    return __awaiter(this, void 0, void 0, function* () {
        ingredientsFiltered = [];
        list_ingredients.forEach((ingredient) => {
            if (ingredient.libelle.toLowerCase().includes(value.toLowerCase())) {
                ingredientsFiltered.push(ingredient);
            }
        });
        fillListIngredients(Tools.dedupe(ingredientsFiltered));
    });
}
const inputTextAppareil = document.querySelector('input[name="appareil"]');
if (inputTextAppareil) {
    inputTextAppareil.addEventListener('keydown', function (e) {
        if (e.target.value.length > 2) {
            filterSearchListAppareil(e.target.value);
        }
        else if (e.target.value.length == 0) {
            fillListAppareils(list_appareils);
        }
        initAddEventListenerTag();
    });
}
function filterSearchListAppareil(value) {
    return __awaiter(this, void 0, void 0, function* () {
        appareilsFiltered = [];
        list_appareils.forEach((appareil) => {
            if (appareil.libelle.toLowerCase().includes(value.toLowerCase())) {
                appareilsFiltered.push(appareil);
            }
        });
        fillListAppareils(Tools.dedupe(appareilsFiltered));
    });
}
const inputTextUstencile = document.querySelector('input[name="ustencile"]');
if (inputTextUstencile) {
    inputTextUstencile.addEventListener('keydown', function (e) {
        if (e.target.value.length > 2) {
            filterSearchListUstencile(e.target.value);
        }
        else if (e.target.value.length == 0) {
            fillListUstenciles(list_ustenciles);
        }
        initAddEventListenerTag();
    });
}
function filterSearchListUstencile(value) {
    return __awaiter(this, void 0, void 0, function* () {
        ustencilesFiltered = [];
        list_ustenciles.forEach((ustencile) => {
            if (ustencile.libelle.toLowerCase().includes(value.toLowerCase())) {
                ustencilesFiltered.push(ustencile);
            }
        });
        fillListUstenciles(Tools.dedupe(ustencilesFiltered));
    });
}
let list_ingredients = [];
let list_appareils = [];
let list_ustenciles = [];
function resetListWithRecettesFiltered(recettesFiltered) {
    recettesFiltered.forEach((recette) => {
        const tmp_ingredients = [];
        recette.ingredients.forEach((ingredient) => {
            tmp_ingredients.push(ingredient);
        });
        list_ingredients = list_ingredients.concat(tmp_ingredients);
        const tmp_appareil = recette.appareil;
        list_appareils = list_appareils.concat(tmp_appareil);
        const tmp_ustenciles = [];
        recette.ustenciles.forEach((ustencile) => {
            tmp_ustenciles.push(ustencile);
        });
        list_ustenciles = list_ustenciles.concat(tmp_ustenciles);
    });
    list_ingredients = Tools.dedupe(list_ingredients);
    list_appareils = Tools.dedupe(list_appareils);
    list_ustenciles = Tools.dedupe(list_ustenciles);
    /*recettesFiltered.map((recette: any) => {
        const tmp_ingredients = recette.ingredients.map((ingredient: any) => {
            return ingredient;
        })
        list_ingredients = list_ingredients.concat(tmp_ingredients);
    
        const tmp_appareil = recette.appareil;
        list_appareils = list_appareils.concat(tmp_appareil);
    
        const tmp_ustenciles = recette.ustenciles.map((ustencile: any) => {
            return ustencile;
        })
        list_ustenciles = list_ustenciles.concat(tmp_ustenciles);
    });*/
    fillListIngredients(list_ingredients);
    fillListAppareils(Tools.dedupe(list_appareils));
    fillListUstenciles(Tools.dedupe(list_ustenciles));
}
/* ADD OR REMOVE TAG IN SEARCH */
function initRemoveEventListenerTag() {
    listTagsDeleteIcon = document.querySelectorAll('.delete-tag');
    listTagsDeleteIcon.forEach((deleteIcon) => deleteIcon.addEventListener('click', function (e) {
        removeTag(e.target);
    }));
}
function initAddEventListenerTag() {
    listItem = document.querySelectorAll('.list-item span');
    listItem.forEach((item) => item.addEventListener('click', function (e) {
        addTag(e.target);
    }));
}
const listTagSection = document.querySelector('.list-things-choosed');
function addTag(element) {
    const type_item = element.getAttribute('data-type');
    const tag_value = element.innerHTML;
    const dom_tag = Dom.getDomTag(tag_value, type_item);
    if (listTagSection) {
        listTagSection.insertAdjacentHTML('beforeend', dom_tag);
    }
    initRemoveEventListenerTag();
    filterSearch(tag_value);
}
function removeTag(element) {
    element.parentNode.remove();
}
