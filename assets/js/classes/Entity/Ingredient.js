export class Ingredient {
    constructor(ingredient) {
        this.libelle = ingredient.ingredient;
        this.quantity = ingredient.quantity;
        this.unit = ingredient.unit;
    }
}
