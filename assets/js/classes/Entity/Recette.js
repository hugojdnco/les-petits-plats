export class Recette {
    constructor(recette, ingredients, appareil, ustenciles) {
        this.id = recette.id;
        this.name = recette.name;
        this.servings = recette.servings;
        this.time = recette.time;
        this.description = recette.description;
        this.ingredients = ingredients;
        this.appareil = appareil;
        this.ustenciles = ustenciles;
    }
}
