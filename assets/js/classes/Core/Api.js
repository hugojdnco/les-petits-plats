var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { Ingredient } from './../Entity/Ingredient.js';
import { Recette } from './../Entity/Recette.js';
import { Ustencile } from './../Entity/Ustencile.js';
import { Appareil } from './../Entity/Appareil.js';
import { Tools } from './../Tools.js';
export class Api {
    static getResponse() {
        return __awaiter(this, void 0, void 0, function* () {
            if (Api.responseApi) {
                return Api.responseApi;
            }
            const responseApi = yield fetch(Api.urlApi);
            Api.responseApi = yield responseApi.json();
            return Api.responseApi;
        });
    }
    static getRecettes() {
        return __awaiter(this, void 0, void 0, function* () {
            if (Api.recettes.length != 0) {
                return Api.recettes;
            }
            const response = yield Api.getResponse();
            let allIngredients = [];
            const allAppareils = [];
            let allUstenciles = [];
            response.recipes.forEach((recette) => {
                const ingredients = [];
                recette.ingredients.forEach((ingredient) => {
                    ingredients.push(new Ingredient(ingredient));
                });
                allIngredients = allIngredients.concat(ingredients);
                const appareil = new Appareil(recette.appliance);
                allAppareils.push(appareil);
                const ustenciles = [];
                recette.ustensils.forEach((ustencile) => {
                    ustenciles.push(new Ustencile(ustencile));
                });
                allUstenciles = allUstenciles.concat(ustenciles);
                Api.recettes.push(new Recette(recette, ingredients, appareil, ustenciles));
            });
            Api.ingredients = Tools.dedupe(allIngredients);
            Api.appareils = Tools.dedupe(allAppareils);
            Api.ustenciles = Tools.dedupe(allUstenciles);
            return Api.recettes;
        });
    }
    static getIngredients() {
        return __awaiter(this, void 0, void 0, function* () {
            return Api.ingredients;
        });
    }
    static getUstenciles() {
        return __awaiter(this, void 0, void 0, function* () {
            return Api.ustenciles;
        });
    }
    static getAppareils() {
        return __awaiter(this, void 0, void 0, function* () {
            return Api.appareils;
        });
    }
}
Api.urlApi = './assets/data/recettes.json';
Api.recettes = [];
Api.ingredients = [];
Api.ustenciles = [];
Api.appareils = [];
