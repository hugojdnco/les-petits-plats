export class Dom {
    static getDomRecettes(recettes) {
        let html = '';
        recettes.forEach((recette) => {
            html += `<div class="recette">
            <div class="img-container">
                <img src="./assets/images/recettes/recette1.jpeg" alt="" />
            </div>
            <div class="content-container">
                <div class="header-content">
                    <h2 class="recette-title">
                        ${recette.name}
                    </h2>
                    <div class="recette-time">
                        <i class="fas fa-clock"></i>
                        <span>${recette.time} min</span>
                    </div>
                </div>
                <div class="body-content">
                    <div class="list-ingredients">
                        <ul>`;
            recette.ingredients.forEach((ingredient) => {
                html += `<li><span>${ingredient.libelle} :</span> ${ingredient.quantity ? ingredient.quantity : ''} ${ingredient.unit ? ingredient.unit : ''}</li>`;
            });
            html += `</ul>
                    </div>
                    <div class="recette-text">
                        ${recette.description}
                    </div>
                </div>
            </div>
        </div>`;
        });
        return html;
    }
    static getDomListIngredients(ingredients) {
        let html = '';
        ingredients.forEach((ingredient) => {
            html += `<div class="list-item">
                        <span data-type="ingredient">${ingredient.libelle}</span>
                    </div>`;
        });
        return html;
    }
    static getDomListAppareils(appareils) {
        let html = '';
        appareils.forEach((appareil) => {
            html += `<div class="list-item">
                        <span data-type="appareil">${appareil.libelle}</span>
                    </div>`;
        });
        return html;
    }
    static getDomListUstenciles(ustenciles) {
        let html = '';
        ustenciles.forEach((ustencile) => {
            html += `<div class="list-item">
                        <span data-type="ustencile">${ustencile.libelle}</span>
                    </div>`;
        });
        return html;
    }
    static getDomTag(value, type) {
        let class_tag = '';
        if (type == 'ingredient') {
            class_tag = 'blue';
        }
        else if (type == 'appareil') {
            class_tag = 'green';
        }
        else if (type == 'ustencile') {
            class_tag = 'red';
        }
        let html = '';
        html += `<div class="thing ${class_tag}">
                    <span>${value}</span>
                    <i class="fas fa-times-circle delete-tag" > </i>
                </div>`;
        return html;
    }
}
