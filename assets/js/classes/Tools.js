export class Tools {
    static dedupe(arr, indexToCheck = 'libelle') {
        const obj = {};
        for (let i = 0, len = arr.length; i < len; i++)
            obj[arr[i][indexToCheck]] = arr[i];
        arr = [];
        for (const key in obj)
            arr.push(obj[key]);
        return arr;
    }
    static firstLetterUppercase(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
}
