"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require("./");
/* SEARCH BAR */
const searchIcon = document.querySelector('.fas.fa-search');
const searchInput = document.querySelector('.global-search');
searchIcon.addEventListener('click', function () {
    searchInput.focus();
});
/* SELECTORS */
const arrowSelect = document.querySelectorAll('.selector .input i');
arrowSelect.forEach((selector) => selector.addEventListener('click', function (e) {
    toggleListSelect(e);
}));
function toggleListSelect(event) {
    const listToUse = document.querySelector('.' + event.target.getAttribute('data-list'));
    if (listToUse.classList.contains('hide')) {
        event.target.classList.add('rotated');
        listToUse.classList.remove('hide');
    }
    else {
        event.target.classList.remove('rotated');
        listToUse.classList.add('hide');
    }
}
console.log(_1.Api.getRecettes());
